FROM openjdk:8-jdk-alpine3.7
VOLUME /tmp

ARG JAR_FILE
ADD target/${JAR_FILE} target/spider-house-1.0.jar

EXPOSE 8111

RUN touch target/spider-house-1.0.jar
ENTRYPOINT ["java","-jar","target/spider-house-1.0.jar", "--spring.profiles.active=docker"]
