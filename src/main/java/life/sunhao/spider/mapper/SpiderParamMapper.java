package life.sunhao.spider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import life.sunhao.spider.entity.SpiderParam;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 15:58
 * @Description:
 */
public interface SpiderParamMapper extends BaseMapper<SpiderParam> {
}
