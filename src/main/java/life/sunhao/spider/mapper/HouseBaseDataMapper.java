package life.sunhao.spider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import life.sunhao.spider.entity.HouseBaseData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 11:27
 * @Description:
 */
public interface HouseBaseDataMapper extends BaseMapper<HouseBaseData> {

    boolean replaceBatch(@Param("houseBaseDatas") List<HouseBaseData> houseBaseDatas);

}
