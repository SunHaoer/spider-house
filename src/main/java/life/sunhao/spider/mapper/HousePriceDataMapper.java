package life.sunhao.spider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import life.sunhao.spider.entity.HousePriceData;

import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 13:59
 * @Description:
 */
public interface HousePriceDataMapper extends BaseMapper<HousePriceData> {

    boolean replaceBatch(List<HousePriceData> housePriceDatas);

}
