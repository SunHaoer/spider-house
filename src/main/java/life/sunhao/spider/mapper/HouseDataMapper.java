package life.sunhao.spider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import life.sunhao.spider.entity.HouseData;
import life.sunhao.spider.entity.HousePriceAvgData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 14:26
 * @Description:
 */
public interface HouseDataMapper extends BaseMapper<HouseData> {

    boolean replaceBatch(@Param("houseDatas") List<HouseData> houseDatas);

    List<HousePriceAvgData> analyzeAvg(@Param("communityName") String communityName, @Param("date") String date);

}
