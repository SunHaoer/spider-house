package life.sunhao.spider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import life.sunhao.spider.entity.HousePriceAvgData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/16 13:41
 * @Description:
 */
public interface HousePriceAvgDataMapper extends BaseMapper<HousePriceAvgData> {

    boolean replaceBatch(@Param("priceAvgDatas") List<HousePriceAvgData> priceAvgDatas);

}
