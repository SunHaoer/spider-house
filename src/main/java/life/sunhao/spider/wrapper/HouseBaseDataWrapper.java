package life.sunhao.spider.wrapper;

import life.sunhao.spider.entity.HouseBaseData;
import life.sunhao.spider.entity.HouseData;
import org.springframework.beans.BeanUtils;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 13:50
 * @Description:
 */
public class HouseBaseDataWrapper extends BaseEntityWrapper<HouseData, HouseBaseData> {

    public static HouseBaseDataWrapper build() {
        return new HouseBaseDataWrapper();
    }

    @Override
    public HouseBaseData entityVO(HouseData entity) {
        HouseBaseData houseBaseData = new HouseBaseData();
        BeanUtils.copyProperties(entity, houseBaseData);
        return houseBaseData;
    }

}
