package life.sunhao.spider.wrapper;

import life.sunhao.spider.entity.HousePriceData;
import life.sunhao.spider.entity.HouseData;
import org.springframework.beans.BeanUtils;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 13:50
 * @Description:
 */
public class HousePriceDataWrapper extends BaseEntityWrapper<HouseData, HousePriceData> {

    public static HousePriceDataWrapper build() {
        return new HousePriceDataWrapper();
    }

    @Override
    public HousePriceData entityVO(HouseData entity) {
        HousePriceData housePriceData = new HousePriceData();
        BeanUtils.copyProperties(entity, housePriceData);
        return housePriceData;
    }

}
