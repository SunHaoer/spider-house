package life.sunhao.spider.spider;

import life.sunhao.spider.common.enums.SourceEnums;
import life.sunhao.spider.common.exception.BizExceptionEnums;
import life.sunhao.spider.common.exception.BussinessException;
import life.sunhao.spider.entity.HouseData;
import life.sunhao.spider.entity.SpiderParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 15:51
 * @Description: 贝壳(ke)
 */
@Slf4j
//@Component
@AllArgsConstructor
public class KeSpider extends BaseSpider implements IHouseSpider {

    @Override
    public String spiderName() {
        return SourceEnums.BEIKE.getValue();
    }

    @Override
    public List<SpiderParam> urls(SpiderParam param) {
        Document document = super.document(param.getUrlPrefix() + param.getUrlPostfix(), SourceEnums.BEIKE.getHost(), 0);
        if(document == null) {
            throw new BussinessException(BizExceptionEnums.SPIDER_CONNECT_FAIL);
        }
        Elements hrefElements = document.getElementsByClass("pagination_group_a").first().select("a[href]");
        List<SpiderParam> result = new ArrayList<>();
        for(Element item : hrefElements) {
            String href = item.attr("href");
            if(StringUtil.isBlank(href)) {
                continue;
            }
            result.add(new SpiderParam(param, href));
        }

        return result;
    }

    @Override
    public List<HouseData> action(SpiderParam param) {
        // 列表页
        Document document = super.document(param.getUrlPrefix() + param.getUrlPostfix(), SourceEnums.BEIKE.getHost(),  0);
        if(document == null) {
            throw new BussinessException(BizExceptionEnums.SPIDER_CONNECT_FAIL);
        }
        Element sellListContent = document.getElementsByClass("sellListContent").first();

        List<HouseData> houseDatas = new ArrayList<>();
        for(Element element : sellListContent.children()) {
            try {
                if(element.getElementsByClass("info clear").first() == null) {
                    // 没有想要的数据
                    continue;
                }
                // 跳转链接
                String url = element.select("a[href]").first().attr("href");
                // 详情页面
                Document infoDocument = super.document(url, SourceEnums.BEIKE.getHost(), 0);
                if(document == null) {
                    log.info("这链接没数据, 跳过");
                    continue;
                }

                // 解析数据
                HouseData dto = new HouseData(param.getTargetName(), param.getRegion(), SourceEnums.BEIKE.getValue());
                dto.setLink(url);
                // 价格
                Element priceElement = infoDocument.getElementsByClass("price").first();
                // 单价
                Integer unitPrice = Integer.parseInt(priceElement.getElementsByClass("unitPriceValue").text());
                dto.setUnitPrice(unitPrice);
                // 总价
                Integer totalPrice = Integer.parseInt(priceElement.getElementsByClass("total").text());
                dto.setTotalPrice(totalPrice);
                // 贝壳编号
                Element houseRecordElement = infoDocument.getElementsByClass("houseRecord").first();
                String agentNo = houseRecordElement.text().split(" ")[1];
                dto.setAgentNo(agentNo);
                // 面积
                Element areaElement = infoDocument.getElementsByClass("area").first();
                String areaStr = areaElement.text().split(" ")[0];
                Double area = Double.parseDouble(areaStr.substring(0, areaStr.length() - 2));
                dto.setArea(area);
                // 楼层
                Element floorElement = infoDocument.getElementsByClass("room").first();
                String floor = floorElement.getElementsByClass("subInfo").text();
                if(floor.length() < 8) {
                    log.info(floor + "不是高层, 跳过");
                    continue;
                }
                dto.setFloor(floor);

                // 其他信息
                Element otherInfoElement = infoDocument.getElementsByClass("m-content").first();
                otherInfoElement = otherInfoElement.getElementsByClass("transaction").first().getElementsByClass("content").first();
                // 房源核验编码
                Element registerNoElement = otherInfoElement.getElementsByClass("register-no").first();
                String registerNo = registerNoElement.text().split(" ")[0].substring(8);
                dto.setRegisterNo(registerNo);
                // 挂牌时间
                String hangTime = otherInfoElement.select("li").get(0).text().substring(4);
                dto.setHangTime(hangTime);
                // 使用性质
                String property = otherInfoElement.select("li").get(3).text();
                if(!property.contains("住宅")) {
                    log.info("不统计非住宅, 跳过");
                    continue;
                }

                houseDatas.add(dto);

                // 休息一下
                super.sleep(SourceEnums.BEIKE.getSleepTime());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return houseDatas;
    }

}
