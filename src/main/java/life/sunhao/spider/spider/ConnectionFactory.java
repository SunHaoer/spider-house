package life.sunhao.spider.spider;

import life.sunhao.spider.common.enums.HeaderEnums;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 16:10
 * @Description:
 */
public class ConnectionFactory {

    public static Connection initConnection(String url, String host, String referer, String userAgent) {
        Connection connection = Jsoup.connect(url);
        connection.header(HeaderEnums.ACCEPT.getKey(), HeaderEnums.ACCEPT.getValue());
        connection.header(HeaderEnums.ACCEPT_ENCODING.getKey(), HeaderEnums.ACCEPT_ENCODING.getValue());
        connection.header(HeaderEnums.CONNECTION.getKey(), HeaderEnums.CONNECTION.getValue());
        connection.header(HeaderEnums.COOKIE.getKey(), null);
        connection.header(HeaderEnums.HOST.getKey(), host);
        connection.header(HeaderEnums.ORIGIN.getKey(), referer);
        connection.header(HeaderEnums.REFERER.getKey(), referer);
        connection.header(HeaderEnums.SEC_CH_UA.getKey(), HeaderEnums.SEC_CH_UA.getValue());
        connection.header(HeaderEnums.SEC_CH_UA_MOBILE.getKey(), HeaderEnums.SEC_CH_UA_MOBILE.getValue());
        connection.header(HeaderEnums.SEC_FETCH_DEST.getKey(), HeaderEnums.SEC_FETCH_DEST.getValue());
        connection.header(HeaderEnums.SEC_FETCH_MODE.getKey(), HeaderEnums.SEC_FETCH_MODE.getValue());
        connection.header(HeaderEnums.SEC_FETCH_SITE.getKey(), HeaderEnums.SEC_FETCH_SITE.getValue());
        connection.header(HeaderEnums.USER_AGENT.getKey(), userAgent);
        connection.header(HeaderEnums.X_REQUESTED_WITH.getKey(), HeaderEnums.X_REQUESTED_WITH.getValue());
        connection.header(HeaderEnums.UPGRADE_INSECURE_REQUESTS.getKey(), HeaderEnums.UPGRADE_INSECURE_REQUESTS.getValue());

        return connection;
    }

}
