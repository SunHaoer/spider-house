package life.sunhao.spider.spider;

import life.sunhao.spider.entity.HouseData;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 09:13
 * @Description:
 */
public interface IHouseSpider extends ISpider<HouseData> {
}
