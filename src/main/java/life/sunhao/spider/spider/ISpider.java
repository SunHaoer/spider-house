package life.sunhao.spider.spider;

import life.sunhao.spider.entity.SpiderParam;

import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 16:59
 * @Description:
 */
public interface ISpider<T> {

    String spiderName();

    List<SpiderParam> urls(SpiderParam param);

    List<T> action(SpiderParam param);

}
