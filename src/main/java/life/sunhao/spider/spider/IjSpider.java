package life.sunhao.spider.spider;

import life.sunhao.spider.common.enums.SourceEnums;
import life.sunhao.spider.common.exception.BizExceptionEnums;
import life.sunhao.spider.common.exception.BussinessException;
import life.sunhao.spider.entity.HouseData;
import life.sunhao.spider.entity.SpiderParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/11 14:14
 * @Description: 我爱我家(5i5j)
 */
@Slf4j
//@Component
@AllArgsConstructor
public class IjSpider extends BaseSpider implements IHouseSpider {

    @Override
    public String spiderName() {
        return SourceEnums.IJ.getValue();
    }

    @Override
    public List<SpiderParam> urls(SpiderParam param) {
        List<SpiderParam> result = new ArrayList<>();
        // 加入第一页
        result.add(param);
        Document document = super.document(param.getUrlPrefix() + param.getUrlPostfix(), SourceEnums.IJ.getHost(), 0);
        if(document == null) {
            throw new BussinessException(BizExceptionEnums.SPIDER_CONNECT_FAIL);
        }

        Element pageElements = document.getElementsByClass("pageSty rf").first();
        if(pageElements == null) {
            // 这个楼盘只有一页
            return result;
        }
        Elements hrefElements = pageElements.select("a");
        for(int i = 0; i < hrefElements.size(); i++) {
            if(i == 0 || i == hrefElements.size() - 1) {
                // 去掉头和尾
                continue;
            }
            Element item = hrefElements.get(i);
            String href = item.attr("href");
            if(StringUtil.isBlank(href)) {
                continue;
            }
            result.add(new SpiderParam(param, href));
        }

        return result;
    }

    @Override
    public List<HouseData> action(SpiderParam param) {
        // 列表页
        Document document = super.document(param.getUrlPrefix() + param.getUrlPostfix(), SourceEnums.IJ.getHost(), 0);
        if(document == null) {
            throw new BussinessException(BizExceptionEnums.SPIDER_CONNECT_FAIL);
        }
        Elements pListElements = document.getElementsByClass("pList").first().select("li");

        List<HouseData> houseDatas = new ArrayList<>();
        for(Element element : pListElements) {
            try {
                String href = element.select("a").get(0).attr("href");
                // 详情页面
                Document infoDocument = super.document(param.getUrlPrefix() + href, SourceEnums.IJ.getHost(), 0);
                if(document == null) {
                    throw new BussinessException(BizExceptionEnums.SPIDER_CONNECT_FAIL);
                }

                // 解析数据
                HouseData dto = new HouseData(param.getTargetName(), param.getRegion(), SourceEnums.IJ.getValue());
                dto.setLink(param.getUrlPrefix() + href);
                // 使用性质
                String otherInfoStr = infoDocument.getElementsByClass("clear jysx").first().text();
                if(!otherInfoStr.contains("住宅")) {
                    log.info("不统计非住宅, 跳过");
                    continue;
                }
                // 我爱我家编号
                String houseIdStr = infoDocument.getElementsByClass("del-houseid").first().text();
                String agentNo = houseIdStr.split("[|]")[0].split("：")[1].trim();
                dto.setAgentNo(agentNo);
                // 房源核验编码
                String registerNo = infoDocument.getElementsByClass("hy_code").text();
                dto.setRegisterNo(registerNo);

                // 总价
                Integer totalPrice = Integer.parseInt(infoDocument.getElementsByClass("de-price fl").select("span").text());
                dto.setTotalPrice(totalPrice);
                // 单价
                String unitPriceStr = infoDocument.getElementsByClass("danjia").first().select("span").first().text();
                Integer unitPrice = new Double(Double.parseDouble(unitPriceStr) * 10000).intValue();
                dto.setUnitPrice(unitPrice);
                // 楼层
                String floor = infoDocument.getElementsByClass("jlyoubai fl jlyoubai1").first().text().split(" ")[1];
                if(floor.length() < 7) {
                    log.info(floor + "不是高层, 跳过");
                    continue;
                }
                dto.setFloor(floor);
                // 面积
                String areaStr = infoDocument.getElementsByClass("jlyoubai fl jlyoubai2").first().text().split(" ")[0];
                areaStr = areaStr.substring(0, areaStr.length() - 2);
                dto.setArea(Double.parseDouble(areaStr));
                houseDatas.add(dto);

                // 休息一下
                super.sleep(SourceEnums.IJ.getSleepTime());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return houseDatas;
    }

}
