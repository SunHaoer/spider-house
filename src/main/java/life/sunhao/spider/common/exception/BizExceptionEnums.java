package life.sunhao.spider.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Auther: hsun
 * @Date: 2021/6/11 16:05
 * @Description:
 */
@Getter
@AllArgsConstructor
public enum BizExceptionEnums {

    SPIDER_CONNECT_FAIL(100,"爬虫连接失败");

    private int friendlyCode;

    private String friendlyMsg;

}
