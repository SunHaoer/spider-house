package life.sunhao.spider.common.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: hsun
 * @Date: 2021/6/11 16:04
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BussinessException extends RuntimeException {

    //友好提示的code码
    private int friendlyCode;

    //友好提示
    private String friendlyMsg;

    public BussinessException(BizExceptionEnums bizExceptionEnums) {
        this.friendlyCode = bizExceptionEnums.getFriendlyCode();
        this.friendlyMsg = bizExceptionEnums.getFriendlyMsg();
    }

}
