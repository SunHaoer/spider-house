package life.sunhao.spider.common.enums;

import lombok.Getter;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 20:06
 * @Description:
 */
@Getter
public enum  SourceEnums {

    BEIKE("贝壳", "ke", "hz.ke.com", 0L),
    IJ("我爱我家", "5i5j", "hz.5i5j.com", 15000L);

    private String key;

    private String value;

    private String host;

    private Long sleepTime;

    SourceEnums(String key, String value, String host, Long sleepTime) {
        this.key = key;
        this.value = value;
        this.host = host;
        this.sleepTime = sleepTime;
    }

}
