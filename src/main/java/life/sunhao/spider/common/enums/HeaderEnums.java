package life.sunhao.spider.common.enums;

import lombok.Getter;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 16:14
 * @Description:
 */
@Getter
public enum HeaderEnums {

    ACCEPT("Accept", "application/json, text/javascript, */*; q=0.01"),
    ACCEPT_ENCODING("Accept-Encoding", "gzip, deflate, br"),
    ACCEPT_LANGUAGE("Accept-Language", "zh-CN,zh;q=0.9"),
    CONNECTION("Connection", "keep-alive"),
    COOKIE("Cookie", "PHPSESSID=b1dudnh5eql5hvssqdle6ej193; domain=hz; historyCity=%5B%22%5Cu676d%5Cu5dde%22%5D; HMF_CI=c4c4a4d48bc8923f0c8788e6a81fe5369c90c7abfd0cd7922f3f264dfbfd752702; _ga=GA1.2.893511507.1623391465; _gid=GA1.2.867499988.1623391465; gr_user_id=963565e2-b3c3-4a96-be10-231fde4d7408; _dx_uzZo5y=984d050a38b27886a6cf72d881b74b61e234190f404295fc92566adc5bfe04847934546d; Hm_lvt_94ed3d23572054a86ed341d64b267ec6=1623391465; smidV2=20210611140425166121115db8e45bea138dc94f0e7c9c005effca22af6d480; __tea_cookie_tokens_221025=%257B%2522web_id%2522%253A%25226972413240408868363%2522%252C%2522ssid%2522%253A%2522a23d9be0-5fb0-4ec2-8a21-c5c568fcdca1%2522%252C%2522user_unique_id%2522%253A%25226972413240408868363%2522%252C%2522timestamp%2522%253A1623391465376%257D; _Jo0OQK=19805472DA9A0C0032B93320B4052FF4C23178F42533C74FB56D0F08C3722E944BA4E832DF1F3E187DC800D157E63FE93059DDCFB66ECE4DFD1EB50F436F358AF15C57212F12283777C98FB9E3C853EFEE298FB9E3C853EFEE215D8BEE34E43E5C0GJ1Z1ag==; banner_A=block; banner_B=none; yfx_c_g_u_id_10000001=_ck21061114043216337776662772777; __TD_deviceId=H340P8JLB1JB5HLJ; ershoufang_cookiekey=%5B%22%257B%2522url%2522%253A%2522%252Fershoufang%252F_%2525E8%25258E%2525B1%2525E8%252592%252599%2525E6%2525B0%2525B4%2525E6%2525A6%2525AD%2525E5%2525B1%2525B1%253Fzn%253D%25E8%258E%25B1%25E8%2592%2599%25E6%25B0%25B4%25E6%25A6%25AD%25E5%25B1%25B1%2526searchtype%253D3%2522%252C%2522x%2522%253A%2522120.0504%2522%252C%2522y%2522%253A%252230.08712%2522%252C%2522name%2522%253A%2522%25E5%25B0%258F%25E5%258C%25BA%25E8%258E%25B1%25E8%2592%2599%25E6%25B0%25B4%25E6%25A6%25AD%25E5%25B1%25B1%2522%252C%2522total%2522%253Anull%257D%22%2C%22%257B%2522url%2522%253A%2522%252Fershoufang%252F_%2525E6%2525B1%252587%2525E9%25259B%252585%2525E8%2525BD%2525A9%253Fzn%253D%25E6%25B1%2587%25E9%259B%2585%25E8%25BD%25A9%2526searchtype%253D3%2522%252C%2522x%2522%253A%2522120.28593%2522%252C%2522y%2522%253A%252230.19906%2522%252C%2522name%2522%253A%2522%25E5%25B0%258F%25E5%258C%25BA%25E6%25B1%2587%25E9%259B%2585%25E8%25BD%25A9%2522%252C%2522total%2522%253Anull%257D%22%2C%22%257B%2522url%2522%253A%2522%252Fershoufang%252F_huiyax%253Fzn%253Dhuiyax%2522%252C%2522x%2522%253A%25220%2522%252C%2522y%2522%253A%25220%2522%252C%2522name%2522%253A%2522huiyax%2522%252C%2522total%2522%253A%25220%2522%257D%22%2C%22%257B%2522url%2522%253A%2522%252Fershoufang%252F_%2525E7%252592%25259F%2525E9%25259A%2525BD%2525E5%252590%25258D%2525E9%252582%2525B8%253Fzn%253D%25E7%2592%259F%25E9%259A%25BD%25E5%2590%258D%25E9%2582%25B8%2526searchtype%253D3%2522%252C%2522x%2522%253A%2522120.29439%2522%252C%2522y%2522%253A%252230.19859%2522%252C%2522name%2522%253A%2522%25E5%25B0%258F%25E5%258C%25BA%25E7%2592%259F%25E9%259A%25BD%25E5%2590%258D%25E9%2582%25B8%2522%252C%2522total%2522%253Anull%257D%22%5D; 8fcfcf2bd7c58141_gr_session_id=0bcdee2c-1939-4608-8414-45e9f40bd775; 8fcfcf2bd7c58141_gr_session_id_0bcdee2c-1939-4608-8414-45e9f40bd775=true; ershoufang_BROWSES=90689041%2C90702650%2C90435107%2C90729586; yfx_f_l_v_t_10000001=f_t_1623391472636__r_t_1623391472636__v_t_1623401725160__r_c_0; HMY_JC=372b8498634cbba3032d7493dc53756340e50a4cb49c37a0c2960cc860f7e72e86,; Hm_lpvt_94ed3d23572054a86ed341d64b267ec6=1623402592"),
    HOST("Host", null),
    ORIGIN("origin", null),
    REFERER("Referer", null),
    SEC_CH_UA("sec-ch-ua", " Not;A Brand\";v=\"99\", \"Microsoft Edge\";v=\"91\", \"Chromium\";v=\"91"),
    SEC_CH_UA_MOBILE("sec-ch-ua-mobile", "?0"),
    SEC_FETCH_DEST("Sec-Fetch-Dest", "empty"),
    SEC_FETCH_MODE("Sec-Fetch-Mode", "cors"),
    SEC_FETCH_SITE("Sec-Fetch-Site", "same-origin"),
    USER_AGENT("User-Agent", null),
    X_REQUESTED_WITH("X-Requested-With", "XMLHttpRequest"),
    UPGRADE_INSECURE_REQUESTS("Upgrade-Insecure-Requests", "1");

    private String key;

    private String value;

    HeaderEnums(String key, String value) {
        this.key = key;
        this.value = value;
    }

}
