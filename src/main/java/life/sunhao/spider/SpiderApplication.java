package life.sunhao.spider;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;

@EnableScheduling
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@NacosPropertySource(dataId = "house.yaml", autoRefreshed = true)
public class SpiderApplication {

    @NacosInjected
    private NamingService namingService;

    public static void main(String[] args) {
        SpringApplication.run(SpiderApplication.class, args);
    }

    @PostConstruct
    public void registerService() throws NacosException {
        namingService.registerInstance("spider-house", "127.0.0.1", 8111);
    }

}
