package life.sunhao.spider.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import life.sunhao.spider.common.api.R;
import life.sunhao.spider.entity.HouseData;
import life.sunhao.spider.entity.SpiderParam;
import life.sunhao.spider.service.IHouseDataService;
import life.sunhao.spider.service.ISpiderParamService;
import life.sunhao.spider.spider.IHouseSpider;
import life.sunhao.spider.spider.IjSpider;
import life.sunhao.spider.spider.KeSpider;
import life.sunhao.spider.utils.DateUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 15:39
 * @Description:
 */
@Api(tags = "爬虫启动")
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/spider/start")
public class SpiderStartController {

    final private IHouseSpider keSpider = new KeSpider();

    final private IHouseSpider ijSpider = new IjSpider();

    final private IHouseDataService houseDataService;

    final private ISpiderParamService spiderParamService;

    @ApiOperation("启动爬虫")
    @Scheduled(cron = "0 0 3 ? * *")
    @GetMapping("/action")
    public R<Boolean> action() {

        List<IHouseSpider> spiders = new ArrayList<>(Arrays.asList(keSpider, ijSpider));
        for(IHouseSpider spider : spiders) {
            List<SpiderParam> spiderParams = spiderParamService.listParams(spider.spiderName());

            for(SpiderParam item : spiderParams) {
                if(!item.getEnable()) {
                    continue;
                }
                log.info("------" + item.getTargetName() + " 开始 ------");
                List<SpiderParam> allSpiderParams = new ArrayList<>();
                try {
                    allSpiderParams = spider.urls(item);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                List<HouseData> houseDatas = new ArrayList<>();
                for(SpiderParam spiderParam : allSpiderParams) {
                    try {
                        houseDatas.addAll(spider.action(spiderParam));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if(!CollectionUtils.isEmpty(houseDatas)) {
                    boolean isSuccess = houseDataService.analyzeSave(houseDatas);
                }
                log.info("------" + item.getTargetName() + " 结束 ------");
            }
        }

        boolean isSuccess = houseDataService.analyzeAvg(null, DateUtil.formatDate(new Date()));

        return R.data(true);
    }

    @ApiOperation("计算平均单价")
    @GetMapping("/avg")
    public R<Boolean> analyzeAvg(
            @RequestParam(required = false) String communityName,
            @RequestParam(required = false) String date
    ) {
        boolean isSuccess = houseDataService.analyzeAvg(communityName, date);
        return R.data(true);
    }


}
