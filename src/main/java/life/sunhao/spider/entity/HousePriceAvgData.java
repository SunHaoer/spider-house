package life.sunhao.spider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: hsun
 * @Date: 2021/6/15 19:00
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("house_price_avg_data")
public class HousePriceAvgData extends BaseEntity {

    @ApiModelProperty("小区名")
    private String communityName;

    @ApiModelProperty("来源")
    private String source;

    @ApiModelProperty("平均价格")
    private Integer avgPrice;

    @ApiModelProperty("挂牌数量")
    private Integer total;

    @ApiModelProperty("时间")
    private String date;

}
