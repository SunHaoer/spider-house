package life.sunhao.spider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 11:13
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("house_price_data")
public class HousePriceData extends BaseEntity {

    @ApiModelProperty("中介的房源编号")
    private String agentNo;

    @ApiModelProperty("房源核验编码")
    private String registerNo;

    @ApiModelProperty("房源总价(万元)")
    private Integer totalPrice;

    @ApiModelProperty("房源单价(元)")
    private Integer unitPrice;

    @ApiModelProperty("数据日期")
    private String date;

}
