package life.sunhao.spider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 15:50
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("spider_param")
public class SpiderParam extends BaseEntity {

    @ApiModelProperty("目标")
    private String targetName;

    @ApiModelProperty("爬虫名")
    private String spider;

    @ApiModelProperty("区域")
    private String region;

    private String urlPrefix;

    private String urlPostfix;

    private Boolean enable;

    private Integer sort;

    public SpiderParam(SpiderParam spiderParam, String urlPostfix) {
        this.targetName = spiderParam.getTargetName();
        this.region = spiderParam.region;
        this.urlPrefix = spiderParam.getUrlPrefix();
        this.urlPostfix = urlPostfix;
        this.enable = spiderParam.getEnable();
    }

}
