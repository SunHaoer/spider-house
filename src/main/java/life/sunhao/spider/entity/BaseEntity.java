package life.sunhao.spider.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 14:37
 * @Description:
 */
public class BaseEntity implements Serializable {

    @JsonSerialize(
            using = ToStringSerializer.class
    )
    private Long id;

}
