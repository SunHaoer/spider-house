package life.sunhao.spider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 19:57
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("house_base_data")
public class HouseBaseData extends BaseEntity {

    @ApiModelProperty("小区名")
    private String communityName;

    @ApiModelProperty("区域")
    private String region;

    @ApiModelProperty("来源")
    private String source;

    @ApiModelProperty("中介的房源编号")
    private String agentNo;

    @ApiModelProperty("房源核验编号")
    private String registerNo;

    @ApiModelProperty("面积")
    private Double area;

    @ApiModelProperty("楼层")
    private String floor;

    @ApiModelProperty("挂牌时间")
    private String hangTime;

    @ApiModelProperty("链接")
    private String link;

}
