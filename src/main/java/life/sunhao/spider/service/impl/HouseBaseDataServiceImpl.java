package life.sunhao.spider.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import life.sunhao.spider.entity.HouseBaseData;
import life.sunhao.spider.mapper.HouseBaseDataMapper;
import life.sunhao.spider.service.IHouseBaseDataService;
import org.springframework.stereotype.Service;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 11:29
 * @Description:
 */
@Service
public class HouseBaseDataServiceImpl extends ServiceImpl<HouseBaseDataMapper, HouseBaseData> implements IHouseBaseDataService {
}
