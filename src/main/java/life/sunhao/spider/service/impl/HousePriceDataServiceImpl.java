package life.sunhao.spider.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import life.sunhao.spider.entity.HousePriceData;
import life.sunhao.spider.mapper.HousePriceDataMapper;
import life.sunhao.spider.service.IHousePriceDataService;
import org.springframework.stereotype.Service;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 14:00
 * @Description:
 */
@Service
public class HousePriceDataServiceImpl extends ServiceImpl<HousePriceDataMapper, HousePriceData> implements IHousePriceDataService {
}
