package life.sunhao.spider.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import life.sunhao.spider.entity.SpiderParam;
import life.sunhao.spider.mapper.SpiderParamMapper;
import life.sunhao.spider.service.ISpiderParamService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 16:00
 * @Description:
 */
@Service
public class SpiderParamServiceImpl extends ServiceImpl<SpiderParamMapper, SpiderParam> implements ISpiderParamService {

    @Override
    public List<SpiderParam> listParams(String spiderName) {
        List<SpiderParam> list = this.list(Wrappers.<SpiderParam>query().lambda().eq(SpiderParam::getSpider, spiderName).eq(SpiderParam::getEnable, true).orderByAsc(SpiderParam::getSort));
        return list;
    }

}
