package life.sunhao.spider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import life.sunhao.spider.entity.*;
import life.sunhao.spider.mapper.HouseBaseDataMapper;
import life.sunhao.spider.mapper.HouseDataMapper;
import life.sunhao.spider.mapper.HousePriceAvgDataMapper;
import life.sunhao.spider.mapper.HousePriceDataMapper;
import life.sunhao.spider.service.IHouseDataService;
import life.sunhao.spider.wrapper.HouseBaseDataWrapper;
import life.sunhao.spider.wrapper.HousePriceDataWrapper;
import lombok.AllArgsConstructor;
import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 14:28
 * @Description:
 */
@Service
@AllArgsConstructor
public class HouseDataServiceImpl extends ServiceImpl<HouseDataMapper, HouseData> implements IHouseDataService {

    final private HouseBaseDataMapper houseBaseDataMapper;

    final private HousePriceDataMapper housePriceDataMapper;

    final private HousePriceAvgDataMapper housePriceAvgDataMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean analyzeSave(List<HouseData> houseDatas) {
        // 保存到 house_data
        boolean isSuccess = baseMapper.replaceBatch(houseDatas);
        List<HouseBaseData> houseBaseDatas = HouseBaseDataWrapper.build().listVO(houseDatas);
        isSuccess = houseBaseDataMapper.replaceBatch(houseBaseDatas);
        List<HousePriceData> housePriceDatas = HousePriceDataWrapper.build().listVO(houseDatas);
        isSuccess = housePriceDataMapper.replaceBatch(housePriceDatas);

        return false;
    }

    @Override
    public boolean analyzeAvg(String communityName, String date) {
        List<HousePriceAvgData> housePriceAvgDatas = baseMapper.analyzeAvg(communityName, date);
        boolean isSuccess = housePriceAvgDataMapper.replaceBatch(housePriceAvgDatas);
        return true;
    }

}
