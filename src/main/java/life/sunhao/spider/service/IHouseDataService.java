package life.sunhao.spider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import life.sunhao.spider.entity.HouseData;

import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 14:28
 * @Description:
 */
public interface IHouseDataService extends IService<HouseData> {

    boolean analyzeSave(List<HouseData> houseDatas);

    boolean analyzeAvg(String communityName, String date);

}
