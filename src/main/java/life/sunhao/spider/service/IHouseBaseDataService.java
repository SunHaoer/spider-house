package life.sunhao.spider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import life.sunhao.spider.entity.HouseBaseData;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 11:28
 * @Description:
 */
public interface IHouseBaseDataService extends IService<HouseBaseData> {
}
