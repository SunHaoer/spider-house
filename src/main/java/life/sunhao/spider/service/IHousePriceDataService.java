package life.sunhao.spider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import life.sunhao.spider.entity.HousePriceData;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 13:59
 * @Description:
 */
public interface IHousePriceDataService extends IService<HousePriceData> {
}
