package life.sunhao.spider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import life.sunhao.spider.entity.SpiderParam;

import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/10 15:59
 * @Description:
 */
public interface ISpiderParamService extends IService<SpiderParam> {

    List<SpiderParam> listParams(String spiderName);

}
